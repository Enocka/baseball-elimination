import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FordFulkerson;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.SET;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

public class BaseballElimination{
    private int [] w;
    private int [] l;
    private int [] r;
    private int [][] g;
    private String [] teams;
    private int numberOfTeams;
    private int totalNumberOfVertices;
    private FlowNetwork flowNetwork;
    private FlowEdge flowEdge;
    private FordFulkerson fordFulkerson;

    // create a baseball division from given filename in format specified below
    public BaseballElimination(String filename){
        In in = new In(filename);
        numberOfTeams = Integer.parseInt(in.readLine());
        w = new int[numberOfTeams];
        l = new int[numberOfTeams];
        r = new int[numberOfTeams];
        g = new int[numberOfTeams][numberOfTeams];
        teams = new String[numberOfTeams];
        String[] nextLine;
        int teamCounter = 0;
        while(in.hasNextLine()){
            teams[teamCounter] = in.readString();
            w[teamCounter] = in.readInt();
            l[teamCounter] = in.readInt();
            r[teamCounter] = in.readInt();
            for(int j = 0; j < numberOfTeams; j++){
                g[teamCounter][j] = in.readInt();
            }
            teamCounter++;
        }
    }
    
    // number of teams
    public int numberOfTeams(){
        return numberOfTeams;
    }
    
    // all teams
    public Iterable<String> teams(){
        Stack<String> teamsStack = new Stack<String>();
        for(int i = numberOfTeams; i > 0; i--){
            teamsStack.push(teams[i-1]);
        }
        return teamsStack;
    }
    
    // number of wins for given team
    public int wins(String team){
        //datasets are tiny so can loop instead of like hashmap or sthing
        for(int i = 0; i < numberOfTeams; i++){
            if (team.equals(teams[i])) return w[i];
        }
        return -1;
    }
    
    // number of losses for given team
    public int losses(String team){
        //datasets are tiny so can loop instead of like hashmap or sthing
        for(int i = 0; i < numberOfTeams; i++){
            if (team.equals(teams[i])) return l[i];
        }
        return -1;
    }
    
    // number of remaining games for given team
    public int remaining(String team){
        //datasets are tiny so can loop instead of like hashmap or sthing
        for(int i = 0; i < numberOfTeams; i++){
            if (team.equals(teams[i])) return r[i];
        }
        return -1;
    }
    
    // number of remaining games between team1 and team2
    public int against(String team1, String team2){
        int team1Index=0, team2Index=0;
        //datasets are tiny so can loop instead of like hashmap or sthing
        for(int i = 0; i < numberOfTeams; i++){
            if (team1.equals(teams[i])) team2Index = i;
            if (team2.equals(teams[i])) team2Index = i;
        }
        return g[team1Index][team2Index];
    }
    
    //get the team exit node that results from i
    private int parentNodeI(int i, int j, int numberOfTeams, int teamIndex){
        int combination = (numberOfTeams-1) * (numberOfTeams-2)/2;
        totalNumberOfVertices = combination + numberOfTeams - 1  + 2;
        int lastIndex = totalNumberOfVertices - 1;
        if (i < teamIndex) return lastIndex - 1 - i;
        //subtract another one since we won't count an exit node for the teamindex
        //in study i.e it goes up one index
        else return lastIndex - 1 - i - 1;
    }
    
    //get the team exit node that results from j
    private int parentNodeJ(int i, int j, int numberOfTeams, int teamIndex){
        int combination = (numberOfTeams-1) * (numberOfTeams-2)/2;
        totalNumberOfVertices = combination + numberOfTeams - 1  + 2;
        int lastIndex = totalNumberOfVertices - 1;
        if (j < teamIndex) return lastIndex - 1 - j;
        //subtract another one since we won't count an exit node for the teamindex
        //in study i.e it goes up one index
        else return lastIndex - 1 - j - 1;
    }
    
    
    // is given team eliminated?
    public boolean isEliminated(String team){
        int teamIndex = 0;
        //datasets are tiny so can loop instead of like hashmap or sthing
        for(int i = 0; i < numberOfTeams; i++){
            if (team.equals(teams[i])) teamIndex = i;
        }
       
        int combination = (numberOfTeams-1) * (numberOfTeams-2)/2;
        totalNumberOfVertices = combination + numberOfTeams - 1  + 2;
        flowNetwork = new FlowNetwork(totalNumberOfVertices);
        int outflowFromS = 0;
        int graphNodeCounter = 1;
        for (int i = 0; i < numberOfTeams; i++){
            for (int j = i + 1; j < numberOfTeams; j++){
                
            }
        }
        for (int i = 0; i < numberOfTeams; i++){
            for (int j = i + 1; j < numberOfTeams; j++){
                if (i == teamIndex || j == teamIndex) continue;
                //add edge from s to g[i][j] element
                String key = new String(i+","+j);
                flowEdge = new FlowEdge(0, graphNodeCounter, g[i][j]);
                flowNetwork.addEdge(flowEdge);
                //add edge from g[i][j] element to vertex reping team
                int exitNode = parentNodeI(i, j, numberOfTeams, teamIndex);
                flowEdge = new FlowEdge(graphNodeCounter, exitNode, Double.POSITIVE_INFINITY);
                exitNode = parentNodeJ(i, j, numberOfTeams, teamIndex);
                flowEdge = new FlowEdge(graphNodeCounter, exitNode, Double.POSITIVE_INFINITY);
                graphNodeCounter++;
                outflowFromS += g[i][j];
            }
        }
        //add edge from g[i][j] element to vertex reping team
        graphNodeCounter = 1;
        int numberOfNodesFromLast = 1;
        int indexOfExitNodes = numberOfTeams;
        for (int i = numberOfTeams-2; i > 0; i--){
            for (int j = 0; j < i; j++){
                flowEdge = new FlowEdge(graphNodeCounter, totalNumberOfVertices - numberOfNodesFromLast - 1, Double.POSITIVE_INFINITY);
                flowNetwork.addEdge(flowEdge);
                graphNodeCounter++;
            }
            numberOfNodesFromLast++;
        }
        //add edge from vertex reping team to t
        graphNodeCounter = 1;
        for (int i = 0; i < numberOfTeams; i++){
            if (i == teamIndex) continue;
            flowEdge = new FlowEdge(totalNumberOfVertices-graphNodeCounter-1, totalNumberOfVertices-1, w[teamIndex]+r[teamIndex]-w[graphNodeCounter]);
            flowNetwork.addEdge(flowEdge);
            graphNodeCounter++;
        }
        fordFulkerson = new FordFulkerson(flowNetwork, 0, totalNumberOfVertices-1);
        int totalNumberOfGamesLeft = 0;
        //look for non-filled edge from s
        for(FlowEdge e : flowNetwork.adj(0)){
            if (e.residualCapacityTo(e.to()) > 0) return true;
        }
        return false;
    }
    
    // subset R of teams that eliminates given team; null if not eliminated
    public Iterable<String> certificateOfElimination(String team){
        if (!isEliminated(team)) return null;
        SET<String> certificateOfElimination = new SET<String>();
        //datasets are tiny so can loop instead of like hashmap or sthing
        int teamIndex = numberOfTeams;
        for(int i = 0; i < numberOfTeams; i++){
            if (team.equals(teams[i])) teamIndex = i;
        }
        int graphNodeCounter = 1;
        for (int i = 0; i < numberOfTeams; i++){
            if (i == teamIndex) continue;
            if(fordFulkerson.inCut(totalNumberOfVertices - graphNodeCounter - 1)){
                if (i < teamIndex)
                    certificateOfElimination.add(teams[i]);
                else certificateOfElimination.add(teams[i+1]);
            }
            graphNodeCounter++;
        }
        return certificateOfElimination;
    }
    
    public static void main(String[] args) {
        BaseballElimination division = new BaseballElimination(args[0]);
        for (String team : division.teams()) {
            if (division.isEliminated(team)) {
                StdOut.print(team + " is eliminated by the subset R = { ");
                for (String t : division.certificateOfElimination(team)) {
                    StdOut.print(t + " ");
                }
                StdOut.println("}");
            }
            else {
                StdOut.println(team + " is not eliminated");
            }
        }
    }
}
